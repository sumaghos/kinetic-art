int enablePin=11;
int in1Pin=2;
int in2Pin=3;
int potPin=0;
int statusPin=13;
boolean reverse=HIGH;
int speed=1000;
int dir = 1;

void setup() {
Serial.begin(9600);
pinMode(in1Pin, OUTPUT);
pinMode(in2Pin, OUTPUT);
pinMode(enablePin, OUTPUT);
pinMode(statusPin,OUTPUT);
}

void loop() {
digitalWrite(statusPin,HIGH);
analogWrite(enablePin,speed);
if (dir == 1) {
  digitalWrite(in1Pin,HIGH);
  digitalWrite(in2Pin,LOW);
  dir = 0;
} else {
  digitalWrite(in1Pin,LOW);
  digitalWrite(in2Pin,HIGH);
  dir = 1;
}
delay(3000);
}
